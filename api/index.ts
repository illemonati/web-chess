import express from "express";
import bodyParser from "body-parser";
import { Chess, ChessInstance } from "chess.js";
import http from "http";
import socketIo, { Client, Socket } from "socket.io";

// process.env.DEBUG = "DEBUG=engine,socket.io*";
const app = express();
app.use(bodyParser.json());
const httpServer = http.createServer(app);
const io = socketIo(httpServer);

enum SessionStatus {
    Ongoing,
    Open,
    Finished
}

interface Session {
    id: string;
    white: string | null;
    black: string | null;
    status: SessionStatus;
    game: ChessInstance;
}

interface SessionClients {
    [key: string]: Socket[];
}

const sessions = [] as Session[];
const sessionClients = {} as SessionClients;

function uuidv4() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
        var r = (Math.random() * 16) | 0,
            v = c == "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
}

app.get("/open-sessions", (req, res) => {
    res.json({
        openSessions: sessions.filter(s => s.status === SessionStatus.Open)
    });
});

app.post("/session", (req, res) => {
    const { id } = req.body;
    res.json({
        session: sessions.find(s => s.id === id)
    });
});

console.log(io.sockets);

const assignPlayerId = (session: Session) => {
    if (!session.black) {
        session.black = uuidv4();
        return session.black;
    } else if (!session.white) {
        session.white = uuidv4();
        return session.white;
    } else {
        return null;
    }
};

io.on("connection", socket => {
    socket.emit("connected");
    socket.on("join-session", ({ id, playerId }) => {
        const session = sessions.find(s => s.id === id);
        if (!session) {
            return;
        }
        console.log(session);
        console.log(sessions);
        let returnPlayerId = playerId;
        if (!playerId) returnPlayerId = assignPlayerId(session);
        // if (session) {
        socket.emit("session-joined", {
            session: session,
            playerId: returnPlayerId
        });
        if (!sessionClients[session.id]) {
            sessionClients[session.id] = [];
        }
        sessionClients[session.id].push(socket);

        // }
    });
    socket.on("request-game-state", ({ sessionId }) => {
        const session = sessions.find(s => s.id === sessionId);
        if (!session) return;
        socket.emit("session-game-update", { gameFen: session.game.fen() });
    });
    socket.on("game-move", ({ move, sessionId, userId }) => {
        console.log(move, sessionId, userId);
        const session = sessions.find(s => s.id === sessionId);
        console.log(session);
        const color = move.color === "w" ? "white" : "black";
        console.log(color);
        if (!session) return;
        (() => {
            console.log(1);
            console.log("sc", session[color]);
            if (session[color] !== userId) return;
            console.log(2);
            const serverMove = session.game.move(move.san);
            if (!serverMove) return;
            console.log(3);
        })();
        for (const clientSocket of sessionClients[session.id] || []) {
            clientSocket.emit("session-game-update", {
                gameFen: session.game.fen()
            });
        }
    });
});

app.post("/create-session", (req, res) => {
    const { color } = req.body;
    const playerUUID = uuidv4();
    const newSession: Session = {
        id: uuidv4(),
        status: SessionStatus.Open,
        white: null,
        black: null,

        game: new Chess()
    };
    if (color === "white") {
        newSession.white = playerUUID;
    } else {
        newSession.black = playerUUID;
    }
    sessions.push(newSession);
    res.json({
        session: newSession,
        playerUUID: playerUUID
    });
});

export default {
    path: "/api/",
    handler: httpServer
};
