import SocketIO from "socket.io-client";
import Vue from "vue";
import VueSocketIO from "vue-socket.io";
export default () => {
    Vue.use(
        new VueSocketIO({
            debug: false,
            connection: SocketIO("", { path: "/api/socket.io/" })
        })
    );
};
