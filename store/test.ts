export const state = () => ({
    counter: 0
});

export const mutations = {
    setsCounter(state: { counter: number }, value: number) {
        state.counter = value;
    }
};

export const actions = {
    setCounter(context: { commit: (arg0: string) => void }, val: number) {
        console.log("yes");
        //@ts-ignore
        context.commit("setsCounter", val);
    }
};
