enum SessionStatus {
    Ongoing,
    Open,
    Finished
}

interface Session {
    id: string;
    white: string | null;
    black: string | null;
    status: SessionStatus;
}

interface ChessState {
    session: Session | null;
    playerUUID: string | null;
}

export const state = () =>
    ({
        session: null,
        playerUUID: null
    } as ChessState);

export const mutations = {
    setSession(state: ChessState, session: Session) {
        state.session = session;
    },
    setPlayerUUID(state: ChessState, playerUUID: string) {
        state.playerUUID = playerUUID;
    }
};

export const actions = {};
