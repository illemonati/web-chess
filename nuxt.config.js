import colors from "vuetify/es5/util/colors";
import serveStatic from "serve-static";

export default {
    /*
     ** Nuxt rendering mode
     ** See https://nuxtjs.org/api/configuration-mode
     */
    mode: "universal",
    /*
     ** Nuxt target
     ** See https://nuxtjs.org/api/configuration-target
     */
    target: "server",
    /*
     ** Headers of the page
     ** See https://nuxtjs.org/api/configuration-head
     */
    head: {
        titleTemplate: "%s - " + process.env.npm_package_name,
        title: process.env.npm_package_name || "",
        meta: [
            { charset: "utf-8" },
            {
                name: "viewport",
                content: "width=device-width, initial-scale=1"
            },
            {
                hid: "description",
                name: "description",
                content: process.env.npm_package_description || ""
            }
        ],
        link: [
            { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
            {
                rel: "stylesheet",
                href:
                    "https://unpkg.com/@chrisoakman/chessboardjs@1.0.0/dist/chessboard-1.0.0.min.css"
            }
        ],
        script: [
            {
                src: "https://code.jquery.com/jquery-3.4.1.min.js",
                type: "text/javascript"
            },

            {
                src:
                    "https://unpkg.com/@chrisoakman/chessboardjs@1.0.0/dist/chessboard-1.0.0.min.js",
                defer: "defer",
                crossorigin: "anonymous",
                integrity:
                    "sha384-8Vi8VHwn3vjQ9eUHUxex3JSN/NFqUg3QbPyX8kWyb93+8AC/pPWTzj+nHtbC5bxD"
            }
        ]
    },
    /*
     ** Global CSS
     */
    css: [],
    /*
     ** Plugins to load before mounting the App
     ** https://nuxtjs.org/guide/plugins
     */
    plugins: [{ src: "~/plugins/socket.client.ts" }],
    /*
     ** Auto import components
     ** See https://nuxtjs.org/api/configuration-components
     */
    components: true,
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: ["@nuxt/typescript-build", "@nuxtjs/vuetify"],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        "@nuxtjs/axios",
        "@nuxtjs/pwa"
        // "nuxt-socket-io"
    ],
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    router: {},
    socket: {},

    axios: {},
    // io: {
    //     // module options
    //     sockets: [
    //         {
    //             name: "test",
    //             default: true,
    //             vuex: {
    //                 mutations: [
    //                     {
    //                         setsCounter: "test/setsCounter"
    //                     }
    //                 ],
    //                 actions: [
    //                     {
    //                         setCounter: "test/setCounter"
    //                     }
    //                 ],
    //                 emitBacks: [
    //                     // When "examples/sample" state changes,
    //                     // emit back the event "examples/sample"
    //                     "test/counter"
    //                 ]
    //             }
    //             // url: "http://localhost:3000"
    //         }
    //     ]
    // },
    /*
     ** vuetify module configuration
     ** https://github.com/nuxt-community/vuetify-module
     */
    vuetify: {
        customVariables: ["~/assets/variables.scss"],
        theme: {
            dark: true,
            themes: {
                dark: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3
                }
            }
        }
    },
    /*
     ** Build configuration
     ** See https://nuxtjs.org/api/configuration-build/
     */
    build: {},
    serverMiddleware: [
        "~/api/index.ts",
        {
            path: "/session/img/chesspieces",
            handler: serveStatic(__dirname + "/static/img/chesspieces")
        }
    ]
};
